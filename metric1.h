#ifndef METRIC1_H
#define METRIC1_H

#include <QObject>
#include <QCoreApplication>
#include <QFile>
#include <QTextStream>
#include <QStringList>
#include <QDebug>
#include <QTextCodec>
#include <math.h>

class metric1
{
private:
    QFile *file;
    short numStr;
    short numEmptyStr;
    short numCommentStr;
    short numStrWithComment;
    short commentPercentage;
    short numUniqOperators;
    short numUniqOperands;
    short numOperators;
    short numOperands;
    short numSeparators;
    short treelevel;
    QStringList operators;
    QStringList uniqOperators;
    QStringList uniqOperands;
public:
    metric1();
    void initOperators();
    void setNumStr();
    void setNumEmptyStr();
    void setNumCommentStr();
    void setNumStrWithComment();
    void setCommentPercentage();

    void setNumOperators();
    void setNumOperands();
  //  void calcOperands(QRegExp reg, QString line);

    void setNumSeparators();
    void setMaxTreeLevel();

    QString Display();
};

#endif // METRIC1_H
