#-------------------------------------------------
#
# Project created by QtCreator 2015-11-24T19:42:48
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lab33
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    metric1.cpp

HEADERS  += mainwindow.h \
    metric1.h

FORMS    += mainwindow.ui
