#include "metric1.h"

metric1::metric1()
{
       file = new QFile("D:\\a.cpp");
       numStr = 0;
       numEmptyStr = 0;
       numCommentStr = 0;
       numStrWithComment = 0;
       numUniqOperators = 0;
       numUniqOperands = 0;
       numOperators = 0;
       numOperands = 0;
       numSeparators = 0;
       treelevel = 0;
}


void  metric1::initOperators()
{
    int i = 0;
    QFile op("D:\\operators.txt");

    if (op.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream in(&op);
        while (!in.atEnd())
        {
            operators.push_back(in.readLine());
            i++;
        }
        op.close();
    }
}

void  metric1::setNumStr()
{
    if (file->open(QIODevice::ReadOnly | QIODevice::Text))
    {
       numStr = QTextStream(file).readAll().split('\n').count();
       file->close();
    }

}

void  metric1::setNumOperands()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            QStringList s;
            if(line.contains("+"))
            {
                s = line.split("+");
                if (!uniqOperands.contains(s[0]))
                    uniqOperands.append(s[0]);
                if (!uniqOperands.contains(s[1]))
                    uniqOperands.append(s[1]);
                numOperands+=2;
            }
            if(line.contains("-"))
            {
                s = line.split("-");
                if (!uniqOperands.contains(s[0]))
                    uniqOperands.append(s[0]);
                if (!uniqOperands.contains(s[1]))
                    uniqOperands.append(s[1]);
                numOperands+=2;
            }
            if(line.contains("*"))
            {
                s = line.split("*");
                if (!uniqOperands.contains(s[0]))
                    uniqOperands.append(s[0]);
                if (!uniqOperands.contains(s[1]))
                    uniqOperands.append(s[1]);
                numOperands+=2;
            }
            if(line.contains("/"))
            {
                s = line.split("/");
                if (!uniqOperands.contains(s[0]))
                    uniqOperands.append(s[0]);
                if (!uniqOperands.contains(s[1]))
                    uniqOperands.append(s[1]);
                numOperands+=2;
            }
            if(line.contains("="))
            {
                s = line.split("=");
                if (!uniqOperands.contains(s[0]))
                    uniqOperands.append(s[0]);
                if (!uniqOperands.contains(s[1]))
                    uniqOperands.append(s[1]);
                numOperands+=2;
            }

             numUniqOperands = uniqOperands.toSet().count();
            //Конец подсчёта операндов
        }
        file->close();
    }
}

/*void  metric1::calcOperands(QRegExp reg, QString line)
{
    if (line.contains(reg))
    {
        int pos = reg.indexIn(line);
        if(pos > -1)
        {
            if (!uniqOperands.contains(reg.cap(0)))
                uniqOperands.append(reg.cap(0));
         //   if (!uniqOperands.contains(reg.cap(1)))
         //       uniqOperands.append(reg.cap(1));
        }
        numOperands+=2;
        file->close();
    }
}*/

void  metric1::setNumEmptyStr()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            if(line.trimmed().isEmpty())
                numEmptyStr++;
        }
        file->close();
    }
}

void  metric1::setNumStrWithComment()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            if (line.contains("//"))
               numStrWithComment++;
        }
        file->close();
    }
}

void  metric1::setCommentPercentage()
{
    commentPercentage = numCommentStr*100/numStr;
}

void  metric1::setNumOperators()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            for(int i=0;i<operators.toSet().count();i++)
            {
                if (line.contains(operators[i]))
                {
                    numOperators++;
                    if(!uniqOperators.contains(operators[i]))
                        uniqOperators.append(operators[i]);
                }
            }
            numUniqOperators = uniqOperators.toSet().count();
        }
        file->close();
    }
}

void  metric1::setNumSeparators()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            if (line.contains(";"))
               numSeparators++;
        }
        file->close();
    }
}

void  metric1::setMaxTreeLevel()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            if (line.contains("if") || line.contains("else") ||
                line.contains("else if") || line.contains("goto") ||
                line.contains("case"))
               treelevel++;
        }
        file->close();
    }
}

QString  metric1::Display()
{
    initOperators();
    setNumStr();
    setNumEmptyStr();
    setNumCommentStr();
    setNumStrWithComment();
    setCommentPercentage();

    setNumOperators();
    setNumOperands();

    setNumSeparators();
    setMaxTreeLevel();


    short VocabluaryOperators = numUniqOperators+numSeparators;
    short ProgramLength = numOperands+numOperators;
    short ProgramVolume = (numOperands+numOperators)*log2(numUniqOperands+numUniqOperators);
    short CyklComplexity = 2-4+2*treelevel;
    QString str = "";
    str+= "Количество строк кода: ";
    str+= QString::number(numStr);
    str+= '\n';
    str+="Количество пустых строк: ";
    str+=QString::number(numEmptyStr);
    str+='\n';
    str+="Количество строк с комментариями: ";
    str+=QString::number(numCommentStr);
    str+='\n';
    str+="Количество строк с кодом и комментариями: ";
    str+=QString::number(numStrWithComment);
    str+='\n';
    str+="Процент комментариев: ";
    str+=QString::number(commentPercentage);
    str+='\n';
    str+="Словарь операторов: ";
    str+=QString::number(VocabluaryOperators);
    str+='\n';
    str+= "Общее количество операторов: ";
    str+=QString::number(numOperators);
    str+='\n';
    str+="Словарь операндов: ";
    str+=QString::number(numUniqOperands);
    str+='\n';
    str+="Общее количество операндов: ";
    str+=QString::number(numOperands);
    str+='\n';
    str+="Длина программы: ";
    str+=QString::number(ProgramLength);
    str+='\n';
    str+="Объем программы: ";
    str+=QString::number(ProgramVolume);
    str+='\n';
    str+="Цикломатическая сложность программы: ";
    str+=QString::number(treelevel);
    str+='\n';
    return str;
}

void  metric1::setNumCommentStr()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            if (line.trimmed().startsWith("//"))
               numCommentStr++;
        }
        file->close();
    }
}
